<?php


abstract class TagAttributeExtractor
{
    protected function extractAttributes($tag_value)
    {
        // <editor-fold desc="code">
        $result = [];
        $matches = [];
        preg_match_all('%(_?[\S]{1,})\s?=[\'"]\s?([^\'"]*)\s?[\'"]%s',$tag_value, $matches);

        if(!empty($matches[1]))
        {
            for($i = 0; $i < count($matches[0]); $i++)
            {
                $result[$matches[1][$i]] = $matches[2][$i];
            }
        }

        if(isset($result['href']))
        {
            $result['absoluteHref'] = self::absoluteHref($result['href']);
        }

        return $result;
        // </editor-fold>
    }

    /**
     *
     * Make `http://domain/some/adress` from `../some/adress` or `/some/adress`
     *
     * @param $url
     * @return string
     */
    private static function absoluteHref($url)
    {
        // <editor-fold desc="code">
        $match = [];
        preg_match('%(https?:\/\/)%',$url,$match);
        if(empty($match))
        {
            $url = preg_replace('%(\.\.\/){1,}%','/',$url);
            $url = preg_replace('%^\/%','',$url);
            $url = preg_replace('%\/$%','',$url);
            $url = 'http://' . Config::$base_url . '/' . $url . '/';
        }

        return $url;
        // </editor-fold>
    }

}