<?php


class TextReplacer
{
    /**
     * @var string
     */
    private $_text;
    /**
     * @var array
     */
    private $_tags;
    /**
     * @var Tag
     */
    private $_tag;
    /**
     * @var TagFinderStrategy
     */
    private $_tagFinderStrategy;

    public function __construct($text,$tag)
    {
        // <editor-fold desc="code">
        if($tag instanceof CommonTag)
        {
            $this->_tagFinderStrategy = new CommonTagFinderStrategy();
        }

        if($tag instanceof SelfClosedTag)
        {
            $this->_tagFinderStrategy = new SelfClosedTagFinderStrategy();
        }

        $this->_text = $text;
        $this->_tag = $tag;
        $this->_tags = $this->_tagFinderStrategy->findTags($this->_tag,$this->_text);
        // </editor-fold>
    }

    /**
     * @return Tag
     */
    public function getTag()
    {
        return $this->_tag;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->_text;
    }

    /**
     * @param $_tags
     */
    public function setTags($tags)
    {
        $this->_tags = $tags;
    }

    /**
     * @return array
     */
    public function getTags()
    {
        return $this->_tags;
    }

    public function replaceTagsWithTheirsValues()
    {
        // <editor-fold desc="code">
        if($this->_tag instanceof SelfClosedTag)
        {
            die('Self closed tags has no values');
        }

        foreach($this->_tags as $tag)
        {
            $this->_text = str_replace($tag->getTagBody(),$tag->getContent(),$this->_text);
        }
        // </editor-fold>
    }

    /**
     *
     * Replace a tags with theirs values if their href attribute match one of given
     *
     * @param $urls
     * @return integer
     */
    public function replaceATagsWithValuesIfHref($urls)
    {
        // <editor-fold desc="code">
        $tagsReplaced = 0;
        foreach($this->_tags as $tag)
        {
            foreach($urls as $url)
            {
                //Game of Encodings
                $url = trim(iconv(iconv_get_encoding($url),iconv_get_encoding($tag->absoluteHeader),$url));
                if($tag->absoluteHref == $url)
                {
                    $this->_text = str_replace($tag->getTagBody(),$tag->getContent(),$this->_text);
                    $tagsReplaced++;
                }
            }
        }

        return $tagsReplaced;
        // </editor-fold>
    }

}