<?php

class SelfClosedTagFinderStrategy extends TagAttributeExtractor implements TagFinderStrategy
{
    public function findTags($tag, $text)
    {
        // <editor-fold desc="code">

        $result = [];

        $matches = null;
        preg_match_all('%<'.$tag->getName().'[^>]*\/?\s?>%s', $text,$matches);


        for($i = 0; $i < count($matches[1]); $i++)
        {
            $new_tag = $tag;
            $new_tag->setTagBody($matches[1][$i]);
            $new_tag->setAttributes($this->extractAttributes($new_tag->getTagBody()));

            $result[] = $new_tag;
        }

        return $result;

        // </editor-fold>
    }
}