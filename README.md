# README #

### What is this repository for? ###

Parse database and do actions with html tags

In this version script can process selected DB and find and replace with text all <a> tags with specified href attribute values.

### How do I get set up? ###

* Summary of set up:
Download, edit Config.php and launch Main.php via browser or command line.
* Configuration:
Config.php contains all the settings - DB config and other
* Dependencies:
PHP > 5.4, MySQL > 5.x


### Using ###
- Create some_name.txt file with listed URL`s. Every URL must start with new line.
- Configure app via edit Config.php.
- Edit Main.php: edit file() function attribute to grab your file with URL`s.
- Launch Main.php.
- Script will create some_long_current_datetime_log_file-Queries.txt. It`ll contain all request that needs to be performed to selected database to replace required <a> tags. Copy them and execute on your database.