<?php

class CommonTagFinderStrategy extends TagAttributeExtractor implements TagFinderStrategy
{
    /**
     * @param Tag $tag
     * @param string $text
     * @return array
     */
    public function findTags($tag, $text)
    {
        // <editor-fold desc="code">

        $result = [];

        $matches = null;
        preg_match_all('%((<'.$tag->getName().'[^>]*>)(.*?)</'.$tag->getName().'>)%s', $text,$matches);

        for($i = 0; $i < count($matches[1]); $i++)
        {

            $new_tag = clone $tag;

            $new_tag->setTagBody($matches[1][$i]);
            $new_tag->setOpenTag($matches[2][$i]);
            $new_tag->setContent($matches[3][$i]);

            $new_tag->setAttributes($this->extractAttributes($new_tag->getOpenTag()));

            $result[] = $new_tag;
        }



        return $result;

        // </editor-fold>
    }

}