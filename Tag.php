<?php


abstract class Tag
{
    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }


    /**
     * @return string
     */
    public function getTagBody()
    {
        return $this->tagBody;
    }

    /**
     * @param string $tagBody
     */
    public function setTagBody($tagBody)
    {
        $this->tagBody = $tagBody;
    }
    /**
     * @var string Tag name
     */
    protected $name = null;
    /**
     * @var array Tag attributes: target, style, href, etc.
     */
    protected $attributes = [];

    /**
     * @param array $attributes
     */
    public function setAttributes($attributes)
    {
        $this->attributes = $attributes;
    }
    /**
     * @var string The tag full text value, starting with <, ending with > or />
     */
    protected $tagBody = null;

    /**
     * @param $name
     * @return mixed Returns property value or null if property not exists
     */
    public function __get($name)
    {
        // <editor-fold desc="code">

        if(isset($this->attributes[$name]))
        {
            return $this->attributes[$name];
        }

        return null;

        // </editor-fold>
    }

    /**
     * @param $name
     * @param $value
     */
    public function __set($name,$value)
    {
        // <editor-fold desc="code">

        if(isset($this->attributes[$name]))
        {
            $this->attributes[$name] = $value;
        }
        else
        {
            die('This tag has no ' . $name . ' attribute' . var_dump($this));
        }
        // </editor-fold>
    }


}