<?php

interface TagFinderStrategy
{
    public function findTags($tag,$text);
}