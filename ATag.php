<?php

/**
 * Class ATag
 * To know if link is external or broken just access the link object `external` and `broken` properties
 */
class ATag extends CommonTag
{
    /**
     * @var boolean Indicates if the link is external
     */
    private $_is_external = null;
    /**
     * @var boolean Indicates if the link is broken: request returns 404 code or page content contents specific marker
     */
    private $_is_broken = null;

    /**
     * ATag constructor.
     */
    public function __construct()
    {
        $this->name = 'a';
    }
    
    /**
     * @param $name
     * @return bool|mixed
     */
    public function __get($name)
    {
        // <editor-fold desc="code">

        if ($name !== 'external' AND $name !== 'broken') {
            return parent::__get($name);
        }

        //Some lazy init here
        if ($name === 'external') {
            if (is_null($this->_is_external)) {
                $this->_is_external = $this->_isExternal();
            }

            return $this->_is_external;
        }

        if ($name === 'broken') {
            if (is_null($this->_is_broken)) {
                $this->_is_broken = $this->_isBroken();
            }

            return $this->_is_broken;
        }

        // </editor-fold>
    }

    /**
     * Check if link is external
     * @return false;
     */
    private final function _isExternal()
    {
        // <editor-fold desc="code">

        if (is_null(Config::$base_url)) {
            die('Can`t detect if link is external - base domain is not set in Config class.');
        }

        $link_domain = null;
        preg_match('%https?:\/\/((?:[a-zA-Z0-9](?:[a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6})%', $this->href, $link_domain);
        $link_domain = array_pop($link_domain);

        if ($link_domain !== Config::$base_url) {
            return true;
        } else {
            return false;
        }

        // </editor-fold>
    }

    /**
     *
     * Check if link is broken
     *
     * @return bool|mixed
     * @throws Exception
     */
    private final function _isBroken()
    {
        // <editor-fold desc="code">
        try {
            $crl = curl_init($this->absoluteHref());
            curl_setopt_array($crl, [
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_USERAGENT => 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)',
                    CURLOPT_FOLLOWLOCATION => 1
                ]
            );

            if (!($html = curl_exec($crl)))
            {
                throw new Exception();
            }
            curl_close($crl);

            $has_broken_page_marker = [];
            preg_match('%'.preg_quote(Config::$page_404_marker).'%',$html,$has_broken_page_marker);

            if(!empty($has_broken_page_marker))
            {
                return true;
            }

            return false;

        } catch (Exception $e) {
            return true;
        }
        // </editor-fold>
    }
}