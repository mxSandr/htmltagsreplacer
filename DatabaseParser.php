<?php

class DatabaseParser
{
    /**
     * @var DatabaseConnection|mixed
     */
    private $_connection;
    /**
     * @var string
     */
    private $_currentTableName;
    /**
     * @var string
     */
    private $_currentPkName;
    /**
     * @var string
     */
    private $_currentPkKValue;
    /**
     * @var array
     */
    private $_tables;

    /**
     * @var Tag
     */
    private $_tag;
    /**
     * @var TextReplacer
     */
    private $_textReplacer;
    /**
     * @var
     */
    private $_parseLog;
    /**
     * @var
     */
    private $_totalTagsDetected = 0;
    /**
     * @var
     */
    private $_totalTagsProcessed = 0;

    /**
     * @var
     */
    private $_tagsDetectedInCurrentTable = 0;
    /**
     * @var
     */
    private $_tagsProcessedInCurrentTable = 0;


    /**
     * DatabaseParser constructor.
     * @param $tag Tag
     */
    public function __construct($tag)
    {
        // <editor-fold desc="code">
        $this->_connection = DatabaseConnection::getInstance();
        $this->_setTables();
        $this->_tag = $tag;
        // </editor-fold>
    }

    /**
     * Set `table_name` => PK_name tables array
     */
    private function _setTables()
    {
        // <editor-fold desc="code">
        $result = [];

        $raw_tables = $this->_connection->db->query(
            'SELECT `TABLES`.`TABLE_NAME`,`KEY_COLUMN_USAGE`.`COLUMN_NAME` FROM  `TABLES` 
         LEFT JOIN `KEY_COLUMN_USAGE` on `TABLES`.`TABLE_NAME` = `KEY_COLUMN_USAGE`.`TABLE_NAME` 
         WHERE `TABLES`.`TABLE_SCHEMA` = \'' . Config::$db_name . '\'
         AND `KEY_COLUMN_USAGE`.`CONSTRAINT_NAME` = \'PRIMARY\'
         AND `KEY_COLUMN_USAGE`.`TABLE_SCHEMA` = \'' . Config::$db_name . '\'');

        foreach($raw_tables as $table)
        {
            $result[$table['TABLE_NAME']] = $table['COLUMN_NAME'];
        }

        $this->_tables = $result;
        // </editor-fold>
    }


    /**
     *
     * Processing current DB and do $action
     *
     * @param $action string TextReplacer method name
     * @param $params array parameters for $action method
     */
    public function parse($action,$params)
    {
        // <editor-fold desc="code">
        $this->_logStart();
        $this->_connection->db->select_db(Config::$db_name);
        foreach ($this->_tables as $tableName => $tablePk){
            $this->_currentTableName = $tableName;
            $this->_currentPkName = $tablePk;
            $rows = $this->_connection->db->query('SELECT * FROM `' . $this->_currentTableName . '`');
            $this->_logStartParseTable();
            foreach($rows as $row)
            {
                $updateData = '';
                $this->_currentPkKValue = $row[$this->_currentPkName];

                $updateCommand = 'UPDATE `' . $this->_currentTableName . '`
                SET %updateData%
                WHERE `' . $this->_currentTableName . '`.`' . $this->_currentPkName. '` = ' . $this->_currentPkKValue . ';';

                foreach($row as $colName => $colValue)
                {
                    //Do not change PKeys
                    if($colName === $this->_currentPkName)
                    {
                        continue;
                    }
                    $this->_textReplacer = new TextReplacer($colValue,clone $this->_tag);
                    $this->_tagsDetectedInCurrentTable += count($this->_textReplacer->getTags());
                    $processedTags = $this->_textReplacer->$action($params);
                    if($processedTags > 0)
                    {
                        $updateData .=
                            '`'.$colName.'` = 
                            \'' . mysqli_real_escape_string($this->_connection->db,$this->_textReplacer->getText()) . '\',';
                    }
                    $this->_tagsProcessedInCurrentTable += $processedTags;
                }

                $updateData = substr($updateData,0,strlen($updateData) - 1);
                $updateCommand = str_replace('%updateData%',$updateData,$updateCommand);
                if(!empty($updateData))
                {
                    $this->_connection->db->query($updateCommand);
                }
            }
            $this->_logEndParseTable();
        }
        $this->_logEnd();
        // </editor-fold>
    }

    private function _logStart()
    {
        $this->_parseLog = 'Starting parse: host ' . Config::$db_host . ', database ' . Config::$db_name .' in '. date('l jS \of F Y h:i:s A') .PHP_EOL;
    }

    private function _logStartParseTable()
    {
        $this->_parseLog .= 'Starting parse ' . $this->_currentTableName . ' table' . PHP_EOL;
    }

    private function _logEndParseTable()
    {
        // <editor-fold desc="code">
        $this->_logTagsInTableDetected();
        $this->_logTagsInTableProcessed();
        $this->_totalTagsDetected +=$this->_tagsDetectedInCurrentTable;
        $this->_totalTagsProcessed +=$this->_tagsProcessedInCurrentTable;
        $this->_tagsProcessedInCurrentTable = 0;
        $this->_tagsDetectedInCurrentTable = 0;
        // </editor-fold>
    }

    private function _logTagsInTableDetected()
    {
        $this->_parseLog .=$this->_tagsDetectedInCurrentTable . ' tags detected'. PHP_EOL;
    }

    private function _logTagsInTableProcessed()
    {
        $this->_parseLog .= $this->_tagsProcessedInCurrentTable . ' tags processed' .PHP_EOL;
    }

    private  function _logEnd()
    {
        // <editor-fold desc="code">
        $this->_parseLog .= 'Parse finished at ' .date('l jS \of F Y h:i:s A') .PHP_EOL;
        $this->_parseLog .= 'Total tags detected:  ' . $this->_totalTagsDetected .PHP_EOL;
        $this->_parseLog .= 'Total tags processed:  ' . $this->_totalTagsProcessed .PHP_EOL;
        $this->_createLogFile();
        echo 'log file created';

        // </editor-fold>
    }

    private function _createLogFile()
    {
        // <editor-fold desc="code">
        $new_log_file = fopen(date('l\-jS\-\of-F\-Y\-h\-i\-s\-A').'.txt', "w");
        fwrite($new_log_file, $this->_parseLog);
        fclose($new_log_file);
        // </editor-fold>
    }

}