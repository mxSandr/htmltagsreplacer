<?php

abstract class SelfClosedTag extends Tag
{
    /**
     * Indicates what type tag is
     */
    const SELF_CLOSED = true;

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        // <editor-fold desc="code">
        return parent::__get($name);
        // </editor-fold>
    }
}