<?php
include ('Config.php');
include ('DatabaseConnection.php');
include ('DatabaseParser.php');

include ('Tag.php');
include ('CommonTag.php');
include ('SelfClosedTag.php');
include ('ATag.php');

include ('TagFinderStrategy.php');
include ('TagAttributeExtractor.php');
include ('CommonTagFinderStrategy.php');
include ('SelfClosedTagFinderStrategy.php');

include ('TextReplacer.php');

$urls = file('links.txt');
//Don`t process doubled url`s
$purified_urls = [];
foreach($urls as $url)
{
    $url = trim($url);
    if(!in_array($url,$purified_urls))
    {
        $purified_urls[] = $url;
    }
}

$ATag = new ATag();
$databaseParser = new DatabaseParser($ATag);

$databaseParser->parse('replaceATagsWithValuesIfHref',$purified_urls);
