<?php

abstract class CommonTag extends Tag
{
    /**
     * Indicates what type tag is
     */
    const SELF_CLOSED = false;

    /**
     * @var string Tag text value between <..>this_content</..> braces
     */
    protected $content;

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getOpenTag()
    {
        return $this->open_tag;
    }

    /**
     * @param string $open_tag
     */
    public function setOpenTag($open_tag)
    {
        $this->open_tag = $open_tag;
    }

    /**
     * @var string Open tag. Example: in '<div id="cool"> some text </div>' it`s '<div id="cool">'
     */
    protected $open_tag;

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        // <editor-fold desc="code">

        return parent::__get($name);

        // </editor-fold>
    }
}