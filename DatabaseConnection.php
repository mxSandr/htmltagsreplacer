<?php

class DatabaseConnection
{

    /**
     * DB connection
     */
    public $db;
    /**
     * @var mixed
     */
    private static $_instance = null;

    /**
     * DatabaseConnection constructor.
     */
    private function __construct()
    {
        $this->db = mysqli_connect(Config::$db_host,Config::$db_user,Config::$db_pass,'information_schema');
    }

    /**
     *  Singletone, heh
     */
    protected function __clone()
    {
    }

    /**
     * @return DatabaseConnection|mixed
     */
    static public function getInstance()
    {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
}