<?php


abstract class Config
{
    /**
     * @var string Base url. Example: kraspesok.ru - note that must be without http(s):// and slashes
     */
    public static $base_url ='kraspesok.ru';
    /**
     * @var string Database host
     */
    public static $db_host ='localhost';
    /**
     * @var string Database name
     */
    public static $db_name = 'u209534_pesok';
    /**
     * @var string Database user
     */
    public static $db_user = 'root';
    /**
     * @var string Database password
     */
    public static $db_pass = '';

    public static $page_404_marker = '<title>404 Not Found | Cтудия рисования песком</title>';
}